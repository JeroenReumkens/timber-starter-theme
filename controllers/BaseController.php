<?php

	class BaseController {
		public function __construct() {
			// Show error message if Timber class is undefined.
			$this->checkForActivatedTimber();

			$this->context = Timber::get_context();
			$this->context['menu'] = 'soort van menu';
		}

		public function checkForActivatedTimber() {
			if (!class_exists('Timber')){
				echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
				return;
			}
		}
	}

?>