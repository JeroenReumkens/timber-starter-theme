<?php

	class PostController extends BaseController {

		public function makeArchive($params) {
			$this->context['blog'] = $params['name'];

			// Render
			Timber::render('test.twig', $this->context);
		}
	}

?>