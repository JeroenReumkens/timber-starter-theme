<?php
	// Controllers
	require_once 'controllers/BaseController.php';
	require_once 'controllers/PostController.php';

	// Routes
	require_once 'routes.php';
?>